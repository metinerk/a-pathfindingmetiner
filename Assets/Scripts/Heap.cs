﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Heap<T> where T : IHeapItem<T>
{

    T[] items;
    int currentItemCount;

    public Heap(int maxHeapSize)
    {
        items = new T[maxHeapSize];
    }

    //Adding new items to the heap
    public void Add(T item)
    {
        //we need to compare two items and say which one has the higher priority so we can sort it in the heap.
        //but we are using generic type of T, so we need to use interface to do this.

        item.HeapIndex = currentItemCount;
        items[currentItemCount] = item;
        SortUp(item);
        currentItemCount++;// after adding, need to increment item count.
    }


    //Removing the first item from the heap.
    public T RemoveFirst()
    {
        T firstItem = items[0];
        currentItemCount--;

        //take the last item in the heap and put it on the first place
        items[0] = items[currentItemCount];
        items[0].HeapIndex = 0;
        SortDown(items[0]);

        return firstItem;
    }

    public void UpdateItem(T item)
    {
        SortUp(item);
    }

    public int Count
    {
        get
        {
            return currentItemCount;
        }
    }

    public bool Contains(T item)
    {
        return Equals(items[item.HeapIndex], item);
    }


    //for sorting the first item to the down
    void SortDown(T item)
    {
        while (true)
        {
            int childIndexLeft = item.HeapIndex * 2 + 1; // the formula for finding the left child index -> 2n+1 , n is parent object's index.
            int childIndexRight = item.HeapIndex * 2 + 2; // the formula for finding the right child index -> 2n+2 , n is parent object's index.
            int swapIndex = 0;

            //check if this item does at least have one child on the left.
            if (childIndexLeft < currentItemCount)
            {
                swapIndex = childIndexLeft;


                //check if this item does at least have one child on the right.
                if (childIndexRight < currentItemCount)
                {
                    if (items[childIndexLeft].CompareTo(items[childIndexRight]) < 0)
                    {
                        swapIndex = childIndexRight;
                    }
                }

                if (item.CompareTo(items[swapIndex]) < 0)
                {
                    Swap(item, items[swapIndex]);
                }
                else
                {
                    //if parent already lower from its child items.It is on the correct position.
                    return;
                }
            }
            else
            {
                //if parent doesnt have a child, it also means it is in the correct position.
                return;
            }


        }


    }

    //for sorting the parent and its child items.
    void SortUp(T item)
    {
        // formula of getting the parent index -> (n-1)/2 n is child item.
        int parentIndex = (item.HeapIndex - 1) / 2;

        while (true)
        {
            T parentItem = items[parentIndex];

            //now need to compare parent item and current item.
            //CompareTo -> if its have higher priorty it returns 1, if equal returns 0, if lower returns -1.
            if (item.CompareTo(parentItem) > 0)
            {
                Swap(item, parentItem);
            }
            else
            {
                break;
            }

            parentIndex = (item.HeapIndex - 1) / 2;
        }
    }

    //Swaping two items in the heap
    void Swap(T itemA, T itemB)
    {
        items[itemA.HeapIndex] = itemB;
        items[itemB.HeapIndex] = itemA;

        int itemAIndex = itemA.HeapIndex;// temporary int for memorise itemA.heapIndex
        itemA.HeapIndex = itemB.HeapIndex;
        itemB.HeapIndex = itemAIndex;
    }
}


//Using comparable interface
public interface IHeapItem<T> : IComparable<T>
{
    int HeapIndex
    {
        get;
        set;
    }
}
